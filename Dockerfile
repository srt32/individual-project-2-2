# Use a Rust base image
FROM rust:latest as build

# Set the working directory inside the container
WORKDIR /app

# Copy the Cargo.toml and Cargo.lock files to leverage Docker's layer caching
COPY Cargo.toml Cargo.lock ./

# Copy the source code of your application
COPY src/ ./src/

# Copy the static assets of your application
COPY static/ ./static/

# Build your application
RUN cargo build --release

# Use a newer Debian slim image for the final container to match glibc version
FROM debian:bookworm-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the built executable from the build stage
# Here, we refer to the 'build' stage by its alias and copy the executable to the current WORKDIR (/app)
COPY --from=build /app/target/release/hello-world hello-world

# Copy the static files from the build stage
# This ensures that the static files are available in the final image
COPY --from=build /app/static/ ./static/

# Expose the port your application runs on
EXPOSE 8080

# Command to run your application
CMD ["/app/hello-world"]
