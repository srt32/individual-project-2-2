# Individual Project Two - Horoscope Generator

# Containerized Rust Actix Web Service

This project demonstrates the containerization of a Rust Actix web service using Docker. The application is a simple RESTful API that provides zodiac signs and horoscopes based on user input.

## Requirements

- Containerize a Rust Actix web application.

- Build the Docker image from the source code.

- Run the container locally to serve the Actix web service.



## Deliverables

- `Dockerfile`
- Video: https://www.youtube.com/watch?v=4Ysi5uz-QX4&ab_channel=SuzannaThompson

- Writeup

## Containerization Process

### Building the Docker Image

To build the Docker image, execute the following command in the terminal:

```
docker build --no-cache -t my-rust-actix-app .
```

### Running the Container

To run the newly built image as a container, execute the following command:

```
docker run -p 8080:8080 my-rust-actix-app
```



# Containerized Rust Actix Web Service

This project demonstrates the containerization of a Rust Actix web service using Docker. The application is a simple RESTful API that provides zodiac signs and horoscopes based on user input.

## Containerization Process

The application is containerized using a multi-stage Docker build process:

### Base Image

We start with the official `rust:latest` image to leverage the Rust environment for building the application.

### Build Stage

In this stage:
- The source code and `Cargo.toml` files are copied into the image.
- The application is compiled, producing an executable.

### Final Image

For the final stage:
- We use a `debian:bookworm-slim` base image to keep the image size small.
- The compiled executable from the build stage is copied into this image.

### Static Files

Static files, such as HTML, CSS, and JavaScript, are included in the Docker image to serve the web application. They are placed under the `static/` directory in the project structure.

## Building the Docker Image

To build the Docker image, execute the following command in the terminal:

```
docker build --no-cache -t my-rust-actix-app .
```
