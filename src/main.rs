use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};
use serde::Deserialize;
use actix_files as fs;



#[derive(Deserialize)]
struct Birthday {
    date: String, // Expecting format YYYY-MM-DD
}

#[get("/")]
async fn index() -> impl Responder {
    HttpResponse::Ok().body("Welcome to the Zodiac Sign and Horoscope Service!")
}


#[post("/zodiac")]
async fn zodiac(birthday: web::Json<Birthday>) -> impl Responder {
    let sign = determine_zodiac(&birthday.date);
    let horoscope = get_horoscope(&sign);
    HttpResponse::Ok().json(serde_json::json!({"sign": sign, "horoscope": horoscope}))
}

fn determine_zodiac(date: &str) -> String {
    let parts: Vec<&str> = date.split('-').collect();
    let month = parts.get(1).and_then(|m| m.parse::<u32>().ok()).unwrap_or(0);
    let day = parts.get(2).and_then(|d| d.parse::<u32>().ok()).unwrap_or(0);

    match (month, day) {
        (3, 21..=31) | (4, 1..=19) => "Aries",
        (4, 20..=30) | (5, 1..=20) => "Taurus",
        (5, 21..=31) | (6, 1..=20) => "Gemini",
        (6, 21..=30) | (7, 1..=22) => "Cancer",
        (7, 23..=31) | (8, 1..=22) => "Leo",
        (8, 23..=31) | (9, 1..=22) => "Virgo",
        (9, 23..=30) | (10, 1..=22) => "Libra",
        (10, 23..=31) | (11, 1..=21) => "Scorpio",
        (11, 22..=30) | (12, 1..=21) => "Sagittarius",
        (12, 22..=31) | (1, 1..=19) => "Capricorn",
        (1, 20..=31) | (2, 1..=18) => "Aquarius",
        (2, 19..=29) | (3, 1..=20) => "Pisces",
        _ => "Unknown",
    }
    .to_string()
}

fn get_horoscope(sign: &str) -> &'static str {
    match sign {
        "Aries" => "Today is a good day for new beginnings.",
        "Taurus" => "Patience will lead you to success.",
        "Gemini" => "Remember to tie your shoes today.",
        "Cancer" => "Are you sure you turned the oven off?",
        "Leo" => "I'm sure no one will follow you home tonight",
        "Virgo" => "Ensure nothing besides water runs from your facet.", 
        "Libra" => "Books burn as well as homes.", 
        "Scorpio" => "Be weary of slightly ajar cabinets.", 
        "Sagittarius" => "Did you lock the door?", 
        "Capricorn" => "Checking twice isn't as good as checking thrice.",
        "Aquarius" => "It's later than you think.",
        "Pisces" => "You're perfect <3",
        _ => "Your future is unclear.",
    }
}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new() 
            .service(zodiac) // Directly add the zodiac service
            .service(fs::Files::new("/", "./static").index_file("index.html")) // Serve static files
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}

